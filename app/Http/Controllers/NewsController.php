<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\User;

class NewsController extends Controller
{
    public function index()
    {       
        $news_title = '';
        if(request('category')) {
            $category = Category::firstWhere('slug', request('category'));
            $news_title = 'in' . $category->category_name;
        }

        if(request('author')) {
            $author = User::firstWhere('username', request('author'));
            $news_title = 'by' . $author->username;
        }
        return view('News',[
            "title" => "All Posts",
            "active" => "Posts",
            "news" => News::latest()->filter(request(['search', 'category', 'author']))->paginate(7)->withQueryString()
        ]);
    }

    public function show(News $new)
    { 
        return view('new',[
            "title" => "Single Post",
            "active" => "Post",
            "news" => $new
        ]);
    }
}
