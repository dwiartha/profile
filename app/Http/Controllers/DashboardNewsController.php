<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Category;
use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class DashboardNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.news.index', [
            'news' => News::where('id_user', auth()->user()->id)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.news.create', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'news_title' => 'required|max:255',
            'slug' => 'required',
            'category_id' => 'required',
            'banner' => 'image|file|max:1024',
            'news_content' => 'required'
        ]);

        if($request->file('banner')){
            $validatedData['banner'] = $request->file('banner')->store('post-images');
        }


        $validatedData['id_user'] = auth()->user()->id;
        $validatedData['excerpt'] = Str::limit(strip_tags($request->news_content), 100);
    
        News::create($validatedData);

        return redirect('/dashboard/news')->with('success', 'New post has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return view('dashboard.news.show',[
            'news' => $news
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\New  $New
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('dashboard.news.edit', [
            'news' => $news,
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $new)
    {
        $rules = [
            'news_title' => 'required|max:255',
            'category_id' => 'required',
            'banner' => 'image|file|max:1024',
            'news_content' => 'required'
        ];

        
        if ($request->slug != $new->slug) {
            $rules['slug'] = 'required|unique:news';
        }

        $validatedData = $request->validate($rules);

        
        if($request->file('banner')){
            if($request->oldImage){
                Storage::delete($request->oldImage); 
            }
            $validatedData['banner'] = $request->file('banner')->store('post-images');
        }

        $validatedData['id_user'] = auth()->user()->id;
        $validatedData['excerpt'] = Str::limit(strip_tags($request->news_content), 100);
    
        News::where('id', $new->id)
            ->update($validatedData);

        return redirect('/dashboard/news')->with('success', 'Post has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\New  $new
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if($news->banner){
            Storage::delete($news->banner); 
        }

        News::destroy($news->id);

        return redirect('/dashboard/news')->with('success', 'Post has been deleted!');
    }


    public function checkSlug(Request $request)
    {
        $slug = SlugService::CreateSlug(News::class, 'slug', $request->news_title);
        return response()->json(['slug' => $slug]);
    }
}
