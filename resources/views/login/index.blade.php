
@extends('layouts.main')

@section('container')

  @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
  
    @if(session()->has('loginError'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('loginError') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  @endif
    <Div Class="Wrapper Login">
        <Div Class="Container">
            <Div Class="Col-Left">
                <Div Class="Login-Text">
                    <H2>Welcome!</H2>
                    <P>Create Your Account.<Br>For Free!</P>
                    <A Href="/signup" Class="Btn">Sign Up</A>
                </Div>
            </Div>

            <Div Class="Col-Right">
                <Div Class="Login-Form">
                    <H2>Log in</H2>
                    <Form Action="/login" method="post">
                      @csrf
                        <P>
                            <Label>Email Address<Span>*</Span></Label>
                            <Input Type="email" Placeholder="Email" name="email" Required>
                        </P>
                        @error('email')
                          <div class="invalid-feedback">
                            {{ $message }}
                          </div>
                        @enderror
                        <P>
                            <Label>Password<Span>*</Span></Label>
                            <Input Type="Password" Placeholder="Password" id="password" name="password" Required>
                        </P>
                        <P>
                            <Input Type="Submit" Value="Login">
                        </P>
                    </Form>
                </Div>
            </Div>

        </Div>
    </Div>
@endsection