
@extends('layouts.main')

@section('container')
    <Div Class="Wrapper Login">
        <Div Class="Container">
            <Div Class="Col-Left">
                <Div Class="Login-Text">
                    <H2>Hai!</H2>
                    <P>Already Have Account?</P>
                    <A Href="/login" Class="Btn">Log In</A>
                </Div>
            </Div>

            <Div Class="Col-Right">
                <Div Class="Login-Form">
                    <H2>Sign up</H2>
                    <Form Action="/signup" method="post">
                      @csrf
                        <P>
                            <Label>Username<Span>*</Span></Label>
                            <Input Type="text" Placeholder="Username" id="username" name="username" Required>
                        </P>
                        <P>
                            <Label>Email Address<Span>*</Span></Label>
                            <Input Type="email" Placeholder="Email" id="email" name="email" Required>
                        </P>
                        @error('email')
                          <div class="invalid-feedback">
                            {{ $message }}
                          </div>
                        @enderror
                        <P>
                            <Label>Password<Span>*</Span></Label>
                            <Input Type="Password" Placeholder="Password" id="password" name="password" Required>
                        </P>
                        <P>
                            <Input Type="Submit" Value="Sign In">
                        </P>
                        {{-- <P>
                            <A Href="">Forgot Password?</A>
                        </P> --}}
                    </Form>
                </Div>
            </Div>

        </Div>
    </Div>
@endsection