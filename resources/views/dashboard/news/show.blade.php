@extends('dashboard.layouts.main')

@section('container')

<div class="container">
    <div class="row my-3">
        <div class="col-lg-8">
            <h1 class="mb-3">{{ $news->news_title }}</h1>
        
            <a href="/dashboard/news" class="btn btn-success"><i class="bi bi-arrow-left-circle"></i> Back To All My news </a>
            <a href="/dashboard/news/{{ $news->slug }}/edit" class="btn btn-warning"><i class="bi bi-pencil-square"></i> Edit </a>
                <form action="/dashboard/news/{{ $news->slug }}" method="news" class="d-inline">
                @csrf
                  @method('delete')
                <button class="btn btn-danger" onclick="return confirm('Are you Sure?')"><i class="bi bi-x-circle"></i> Delete </button>
                </form>

                @if ($news->banner)
                <div style="max-height: 350px; overflow:hidden">
                    <img src="{{ asset('storage/' . $news->banner) }}" alt="{{ $news->category->category_name }}" class="img-fluid mt-3">
                </div>
                 @else   
                 <img src="https://source.unsplash.com/1200x400?"{{ $news->category->category_name }} alt="{{ $news->category->category_name }}" class="img-fluid mt-3">
                 @endif
                 
            <article class="my-3 fs-5">
                {!! $news->news_content !!}
            </article>

        </div>
    </div>
</div>

@endsection