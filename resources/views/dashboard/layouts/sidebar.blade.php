<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" aria-current="page" href="/dashboard" style="font-size: 15px">
            <i class="bi bi-house-door" style="font-size: 20px;"></i>
            Dashboard
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ Request::is('dashboard/news*') ? 'active' : '' }}" href="/dashboard/news" style="font-size: 15px">
            <i class="bi bi-card-text" style="font-size: 15px;"></i>
            My Posts
          </a>
        </li>
      </ul>

      @can('admin')
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt4 mt-2 text-muted">
        <span>Administrator</span>
      </h6>
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link {{ Request::is('dashboard/categories*') ? 'active' : '' }}" href="/dashboard/categories" style="font-size: 15px">
            <i class="bi bi-bookmarks" style="font-size: 15px;"></i>
            Post Categories
          </a>
        </li>
      </ul>
      @endcan
    </div>
  </nav>