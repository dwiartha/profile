@extends('layouts.main')

@section('container')
<div class="container">
    <div class="row justify-content-center mb-5">
        <div class="col-md-8">
            <h1 class="mb-3">{{ $news->news_title }}</h1>
        
            <p>By. <a href="/new?author={{ $news->author->username }}" class="text-decoration-none">{{ $news->author->username }}</a> in <a href="/new?category={{ $news->category->slug }}" class="text-decoration-none">{{ $news->category->category_name }}</a></p>
            
            @if ($news->banner)
            <div style="max-height: 350px; overflow:hidden">
                <img src="{{ asset('storage/' . $news->banner) }}" alt="{{ $news->category->category_name }}" class="img-fluid">
            </div>
             @else   
             <img src="https://source.unsplash.com/1200x400?"{{ $news->category->category_name }} alt="{{ $news->category->category_name }}" class="img-fluid">
             @endif

            <article class="my-3 fs-5">
                {!! $news->news_content !!}
            </article>
        <a href="/news" class="display-block mt-5 text-decoration-none">Back to Post</a>    
        </div>
    </div>
</div>
@endsection