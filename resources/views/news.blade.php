@extends('layouts.main')

@section('container')
<h1 class="mb-3 text-center">{{ $title }}</h1>

  <div class="row justify-content-center md-3">
    <div class="col-md-6">
      <form action="/news">
        @if (request('category'))
          <input type="hidden" name="category" value="{{ request('category') }}">
        @endif
        @if (request('author'))
          <input type="hidden" name="author" value="{{ request('author') }}">
        @endif
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Search.." name="search" value="{{ request('search') }}">
          <button class="btn btn-danger" type="submit">Search</button>
        </div>
      </form>
    </div>
  </div>


    @if ($news->count())
    <div class="card mb-3">
      @if ($news[0]->banner)
        <div style="max-height: 400px; overflow:hidden">
          <img src="{{ asset('storage/' . $news[0]->banner) }}" alt="{{ $news[0]->category->category_name }}" class="img-fluid">
        </div>
      @else   
        <img src="https://source.unsplash.com/1200x400?{{ $news[0]->category->category_name }}" class="card-img-top" alt="{{ $news[0]->category->category_name }}">
      @endif
        <div class="card-body text-center">
          <h3 class="card-title"><a href="/news/{{ $news[0]->slug }}" class="text-decoration-none text-dark">{{ $news[0]->news_title }}</a></h3>
          <p>
            <small class="text-muted">By. <a href="/news?author={{ $news[0]->author->username }}" class="text-decoration-none">{{ $news[0]->author->name }}</a> in <a href="/news?category={{ $news[0]->category->slug }}" class="text-decoration-none">{{ $news[0]->category->category_name }}</a> {{ $news[0]->created_at->diffForHumans() }}
            </small>
        </p>
          <p class="card-text">{{ $news[0]->excerpt }}</p>
          <a href="/news/{{ $news[0]->slug }}" 
            class="text-decoration-none btn btn-primary">Read More</a>
        </div>
      </div>

    <div class="container">
        <div class="row">
            @foreach ($news->skip(1) as $new)
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="position-absolute px-3 py-2" style="background-color: rgba(0, 0, 0, 0.3)"><a href="/news?category={{ $new->category->slug }}" class="text-white text-decoration-none">{{ $new->category->category_name }}</a></div>
                    @if ($new->banner)
                    <img src="{{ asset('storage/' . $new->banner) }}" alt="{{ $new->category->category_name }}" class="img-fluid">
                    @else   
                    <img src="https://source.unsplash.com/500x400?{{ $new->category->category_name }}" class="card-img-top" alt="{{ $new->category->category_name }}">
                    @endif
                    <div class="card-body">
                    <h5 class="card-title">{{ $new->news_title }}</h5>
                    <p>
                        <small class="text-muted">By. <a href="/news?author={{ $news[0]->author->username }}" class="text-decoration-none">{{ $new->author->name }}</a> {{ $new->created_at->diffForHumans() }}
                        </small>
                    </p>
                    <p class="card-text">{{ $new->excerpt }}</p>
                    <a href="/news/{{ $new->slug }}" class="btn btn-primary">Read More</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    @else
    <p class="text-center fs-4">No post found.</p> 
    @endif

    <div class="d-flex justify-content-end">
    {{ $news->links() }}
    </div>

@endsection