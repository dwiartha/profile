@extends('layouts.main')

@section('container')
    <h1 class="mb-5">Post Category : {{ $category }}</h1>

    @foreach ($news as $new)
        <h2>
            <a href="/news/{{ $new->slug }}">{{ $new->news_title }}</a>
        </h2>
        <h5>{{ $new->author }}</h5>
        <p>{{ $new->excerpt }}</p>
    @endforeach

@endsection