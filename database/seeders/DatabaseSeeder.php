<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\News;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        
        User::create([
            'name' => 'Kadek Sandy',
            'username' => 'Sandy',
            'email' => 'Kadeksandy1234@gmail.com',
            'password' => bcrypt('12345')
        ]);
        User::create([
            'name' => 'Made Dedy',
            'username' => 'Dedy',
            'email' => 'dedywanditya@gmail.com',
            'password' => bcrypt('54321')
        ]);

        User::factory(3)->create();

        Category::create([
            'id_user' => mt_rand(1,3),
            'category_name'  => 'Web-Programing',
            'slug' => 'web-programing',
            'category_description' => 'web_Programing'
        ]);
        Category::create([
            'id_user' => mt_rand(1,3),
            'category_name'  => 'Web-Design',
            'slug' => 'web-design',
            'category_description' => 'web_design'

        ]);
        Category::create([
            'id_user' => mt_rand(1,3),
            'category_name'  => 'Personal',
            'slug' => 'personal',
            'category_description' => 'personal'
        ]);
        
        // News::factory(20)->create();
    }
}
