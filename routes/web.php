<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SignupController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\DashboardNewsController;
use App\Http\Controllers\AdminCategoryController;
use App\Models\Category;
use App\Models\User;
use App\Models\News;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home',[
        "title" => "Home",
        "active" => "home",
    ]);
});

Route::get('/about', function () {
    return view('about',[
        "title" => "About",
        "active" => "about",
        "name" => "Kadek Sandy",
        "email" => "kadeksandy1234@gmail.com"
    ]);
});
Route::get('/news', [NewsController::class, 'index']);

Route::get('news/{new:slug}', [NewsController::class, 'show']);

Route::get('/category', function () {
    return view('Category', [
        "title" => "Category",
        "active" => "category",
        "category" => Category::all()
    ]);
});

Route::get('/signup', [SignupController::class, 'index'])->name('signup')->middleware('guest');
Route::post('/signup', [SignupController::class, 'store']);


Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);


Route::get('/dashboard', function() {return view('dashboard.index');})->middleware('auth');
// Route::get('/dashboard', [DashboardNewsController::class, 'index'])->middleware('auth');


Route::get('/dashboard/news/checkSlug', [DashboardNewsController::class, 'checkSlug'])->middleware('auth');
Route::resource('/dashboard/news', DashboardNewsController::class )->middleware('auth');



Route::resource('/dashboard/categories', AdminCategoryController::class )->except('show')->middleware('admin');